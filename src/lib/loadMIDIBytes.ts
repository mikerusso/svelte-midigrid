import MIDIFile from 'midifile'


export default async function _load({ page, fetch, session, context }) {
    const url = '/DearPrudence.mid';
    const res = await fetch(url);
    if (res.ok) {
        const blob = await res.blob()
        const buffer = await blob.arrayBuffer()
        const file = new MIDIFile(buffer)
        const tracksCount = file.header.getTracksCount()
        let midiEvents = createSongObject(tracksCount)


        let rawev = file.getMidiEvents()
        let dur = 0
        rawev.forEach(e => {
            if (e.subtype == 8 || e.subtype === 9) {
                // for perf, get everything done at once!
                midiEvents[e.track][e.param1].push(e)
                if(e.playTime>dur) dur=e.playTime
            }
        });

        return {
            props: {
                mididata: midiEvents,
                tcount: tracksCount,
                midbuf: buffer,
                totalDur: dur + 1000 //give a little room for glitchy midifiles
            }
        };
    }

    return {
        status: res.status,
        error: new Error(`Could not load ${url}`)
    };
}

function createSongObject(n: number) {
    let out = {}
    for (let i = 0; i < n; i++) out[i] = createTrackObject()
    return out
}

function createTrackObject() {
    let out = {}
    for (let i = 0; i < 128; i++) out[i] = []
    return out

}
