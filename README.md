# svelte Midi Grid ( with wasm timidity synth ) 




## working notes
### project structure
Separating our concerns, we two UI components
- transport: play, pause, load for the grid, will be connected eventually to timidity module
- the grid itself: a table with 128 rows, and one column that can hold array of note events

### thinking ahead to audio first
[timidity-wasm](https://gitlab.com/joegame/timidity-wasm) is, by design, kind of a blackbox api insofar as, you point it to a base directory for the [patch files](https://gitlab.com/joegame/gravisPats), load it the raw bytes of midi, and you can play, pause, and query progress.  Theoretically, we could load timidity and load the UI in parallel, so that the same bytes render the grid and feed the synth, but I think if we ever want to have some _editing_ flexbility in this app we should instead consider an intermediary representation of the midi, which can be a big prop for the whole app.

Need a midi parser....  [this](https://www.npmjs.com/package/midi-json-parser)  looks nice, but we need to be able to go from json to bytes again.  

Ok we will use [MIDIFile](https://github.com/nfroidure/MIDIFile) because it can decode/encode.  Its not so well maintained but its fine for now.

### what is the hierarchy?
right now we have a hardcoded midi file loaded from the base page component using the sveltekit `load` function.  But that is probably not the best fit moving forward.  But, as long as we keep that loader logic separate, it should be easy to refactor.

### widths and screen
We will take html's word for it when we say that: if our font size is 4px  and our screensize is, say, 1399px, then our own low-resolution 'tick size' is 4/1399

In that sense, if we then normalize our starts and ends of each event along that tick size, we can iterate through and make it look good.  

I think something where we iterate through the notearr, tentatively assuming their well-orderedness for now,
we can make a small state machine to be like: "ok are we within a note in this tick or not? true ? charSpace : charBox"


``` js
function genNoteRow(arr){
    let inNote = false
    let output = ''

    arr.forEach(note...)
}
```


no no, unless we want to prenormalize each note arr _first_, then go through that kind of iteration, we should think the other way around.

If we already have an array of a single note, we should assume that they all have even num of items in them (for noteOns and then noteOffs) (and this is tentatively confirmed by our testfile).   So instead lets see if we can do this:

``` javascript
function genNoteRow(arr, length){
let output = ''
const firstStart = arr[0].startTime
if(firstStart>0){
let l =getCharLength(firstStart)
output += addChars(l,' ')
}
for(let i=0; i<arr.length-2; i+=2){
    const dur = arr[i+1].startTime - arr[i].starTime
    // normalize dur add note char
    const break = arr[i+2].startTime - arr[i+1].starTime
    // normalize dur add space char
}
}
```

### unicode and font sizes
just screenshotting and putting into gimp: the unicode squares I am using seem to be the sqrt of the defined pixel font size

but... that doesnt seem to be consisten, I think the best thing is just finding the right letter spacing such that we can do the math easily.

And either way with most screen sizes its not enough resolution!


for the joiner "space": 
https://jkorpela.fi/chars/spaces.html
## stages
### v0.0
We will make a svelte app that spits out the json from MIDIFile.  From here the next task can be rendering the grid from our data.

How do assets work in svelte?  I guess we should just consider them endpoints, at least for our uses atm.  

### v0.1
lets start with one track and one note row from the midi file and try to render it with pure unicode line

